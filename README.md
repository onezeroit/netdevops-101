# NetDevOps 101

## Doelstelling
Een introductie in automation voor network engineers. Tijdens dit traject leer je de tools en principes van NetDevOps: het gebruik van scripts en tools om de uitrol en het beheer van netwerken te optimaliseren, versnellen, en betrouwbaarder te maken.

In zes avonden behandelen we onderwerpen als Agile, Infrastructure as Code, CI/CD; we gebruiken de tools Ansible, Salt, NAPALM, Jinja2, en Git. We demonstreren alles met hands-on opdrachten die je zelf direct in een lab kan toepassen.

Aan het einde van het traject begrijp je het landschap aan networkautomation tools, heb je een gereedschapskist waarmee je direct aan de slag kan, en weet je waar je het best kan beginnen om de netwerken waar je voor verantwoordelijk bent direct beter, sneller en slimmer te maken.

## Programma

*  Dag 1: eerste stappen met NetDevOps
*  Dag 2: Jinja2, een taal om configuratietemplates mee te maken
*  Dag 3: NAPALM, een vendor-neutrale manier om het beheer van netwerkappartuur te automatiseren.
*  Dag 4 en 5: Ansible, het zwitserse zakmes onder de automation platforms en de facto industriestandaard voor netwerk automation.
*  Dag 6: Salt, event-driven network orchestration.
