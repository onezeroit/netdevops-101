'''
 get_facts()

    Returns a dictionary containing the following information:

            uptime - Uptime of the device in seconds.
            vendor - Manufacturer of the device.
            model - Device model.
            hostname - Hostname of the device
            fqdn - Fqdn of the device
            os_version - String with the OS version running on the device.
            serial_number - Serial number of the device
            interface_list - List of the interfaces of the device

'''
import napalm

# Use the appropriate network driver to connect to the device:
driver = napalm.get_network_driver("ios")

device = driver(
    hostname="10.1.1.11",
    username="onezero",
    password=None,
    optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
)

# open session to device
print("Opening ...")
device.open()

# execute commands
print("Getting Facts ...")
facts = device.get_facts()

# close the session with the device.
device.close()
print("Done.")

# display results
print("-"*60)
print("{:15}{:15}{:15}{:15}".format("hostname", "serial", "model", "version"))
print("{:15}{:15}{:15}{:15}".format(facts["hostname"], facts["serial_number"], facts["model"], facts["os_version"]))
print("-"*60)
