'''
Oefening 6: set config op basis van Jinja template
    BGP, via loopback (dus ook static routes, delete old neigbors)
    rollback als de neighbors na 30 seconden niet up zijn
'''
import get_inventory
import napalm
import time
from jinja_loader import load_yaml_files

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
user = 'onezero'

# fetch inventory
routers = get_inventory.fetch(tkn)

# set template vars
bgp_template = 'bgp'
template_path = '/home/onezero/avond3'
vars_files = {
  'RTR1': 'oefening6_RTR1.yml',
  'RTR2': 'oefening6_RTR2.yml',
}

def create_device(dev, usr):
    """
    turn router item into proper device dict.
    """
    item = driver(
        hostname=dev['ip_address'],
        username=usr,
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
    )
    return item

# set new BGP config
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = create_device(router, user)
    # load variables
    vars_file = vars_files[router['hostname']]
    template_vars = load_yaml_files(vars_file)  # via functie uit jinja_loader.py -- DRY

    # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # load config
    device.load_template(bgp_template,template_path=template_path, **template_vars)
    # show diff
    print("\nDiff:")
    print(device.compare_config())
    print("Committing ...")
    device.commit_config()
    # close the session with the device.
    device.close()
    print("Done.")

# wait a bit, to allow neighborships to establish
delay = 30
print("waiting {} seconds to allow neighborships to establish".format(delay))
time.sleep(delay)

## Check if neighbors are up -- if not: rollback
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = create_device(router, user)
    # load variables
    vars_file = vars_files[router['hostname']]
    template_vars = load_yaml_files(vars_file)

        # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # execute commands
    print("Gathering Facts")
    bpg_neighbors = device.get_bgp_neighbors()

    for neighbor in template_vars['bgp']['neighbors']:
        if bpg_neighbors['global']['peers'][neighbor['host']]['is_up']:
            print("Neighbor {} is up".format(neighbor['host']))
        else:
            print("Neighbor {} is down, rolling back".format(neighbor['host']))
            device.rollback()
    # close the session with the device.
    print("Closing connection")
    device.close()
