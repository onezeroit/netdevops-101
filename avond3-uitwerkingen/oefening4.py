'''
Oefening4: BGP neighbors
    Verzamel van elke router:
        BGP neighbors:
            IP-adres
            MAC adres
            Uitgaand interface
            Is pingbaar?

'''
import get_inventory
import napalm

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
user = 'onezero'

# get inventory
routers = get_inventory.fetch(tkn)

# initialize results list
results = list()

# iterate over inventory
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = driver(
        hostname=router['ip_address'],
        username=user,
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
    )

    # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # execute commands
    print("Gathering Facts")
    bpg_neighbors = device.get_bgp_neighbors()
    arp_table = device.get_arp_table()
    for neighbor in bpg_neighbors['global']['peers'].items():
        neighbor_ip = neighbor[1]['remote_id']
        # parse arp table
        for arp in arp_table:
            if arp['ip'] == neighbor_ip:
                neighbor_mac = arp['mac']
                neighbor_if = arp['interface']
        # test pingable
        ping = device.ping(neighbor_ip)
        if ping['success']:
            pingable = True
        else:
            pingable = False
    # close the session with the device.
    print("Closing connection")
    device.close()

    # update results list: neighbor IP, MAC, IF, pingable
    result = {
        'host': router['ip_address'],
        'ip': neighbor_ip,
        'mac': neighbor_mac,
        'interface': neighbor_if,
        'pingable': str(pingable),
    }
    results.append(result)

fields = [
    'host',
    'neighbor',
    'mac',
    'interface',
    'pingable',
]
print("-"*80)
print("{:15}{:15}{:20}{:20}{:10}".format(*fields))
for item in results:
    print("{host:15}{ip:15}{mac:20}{interface:20}{pingable:10}".format(**item))
