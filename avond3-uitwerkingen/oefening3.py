'''
Oefening3:
* haal 'facts' uit de devices: hostname, model, serial, os_version, uptime
* print in een mooie tabel
* gebruik inventory data uit Netbox

'''
import get_inventory
import napalm

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
user = 'onezero'

# get inventory
routers = get_inventory.fetch(tkn)

# initialize results list
results = list()

# iterate over inventory
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = driver(
        hostname=router['ip_address'],
        username=user,
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
    )

    # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # execute commands
    print("Gathering Facts")
    facts = device.get_facts()
    # close the session with the device.
    print("Closing connection")
    device.close()

    # update results list: hostname, model, serial, os_version, uptime
    result = {
        'hostname': facts['hostname'],
        'model': facts['model'],
        'serial': facts['serial_number'],
        'version': facts['os_version'],
        'uptime': facts['uptime'],
    }
    results.append(result)

fields = [
    'hostname', 
    'model',
    'serial',
    'uptime',
    'version',
]
print("-"*75)
print("{:15}{:15}{:15}{:15}{:15}".format(*fields))
for item in results:
    print("{hostname:15}{model:15}{serial:15}{uptime:15}{version:15}".format(**item))
