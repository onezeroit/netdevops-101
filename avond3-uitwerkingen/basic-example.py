import requests
import json

headers = {
    'accept': 'application/json',
    'Authorization': 'Token e67b72eb4f37b460d8001906049e4c85b85648ca',
}

netbox_url = 'https://netbox.interestingtraffic.nl/api/'
request_uri = 'dcim/devices'

response = requests.get(netbox_url+request_uri, headers=headers)

results = json.loads(response.text)
print(results)
