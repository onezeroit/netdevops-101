'''
Oefening 5:
* set SNMP configuratie:
    contact: onezero@example.com
    location OneZero Lab environment
    community: 1_0_snmp (RO)
* verifieer met get_snmp_information
'''
import get_inventory
import napalm

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
user = 'onezero'

snmp_settings = {
    'contact': 'onezero@example.com',
    'location': 'OneZero Lab environment',
    'community': '1_0_snmp',
    'type': 'ro'
}

# generate config statements
config = """
snmp-server community {community} {type}
snmp-server contact {contact}
snmp-server location {location}
""".format(**snmp_settings)

# fetch inventory
routers = get_inventory.fetch(tkn)

# initialize results list
results = list()

# iterate over inventory
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = driver(
        hostname=router['ip_address'],
        username=user,
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
    )

    # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # load config
    device.load_merge_candidate(config=config)

    # show diff
    print("\nDiff:")
    print(device.compare_config())

    # You can commit or discard the candidate changes.
    choice = input("\nWould you like to commit these changes? [yN]: ")
    if choice == "y":
        print("Committing ...")
        device.commit_config()
    else:
        print("Discarding ...")
        device.discard_config()

    # validate config
    snmp_config = device.get_snmp_information()
    print("\nValidating SNMP config...")
    print("contact: {}".format(str(snmp_settings['contact']==snmp_config['contact'])))
    print("location: {}".format(str(snmp_settings['location']==snmp_config['location'])))
    print("community: {}".format(str(snmp_settings['type']==snmp_config['community'][snmp_settings['community']]['mode'])))

    # close the session with the device.
    device.close()
    print("Done.")

