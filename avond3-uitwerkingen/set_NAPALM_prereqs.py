'''
Set NAPALM prereqs for IOS
'''
import get_inventory
import napalm

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
user = 'onezero'

# generate config statements
config = """
archive
  path flash:archive
  write-memory
ip scp server enable
"""

# fetch inventory
routers = get_inventory.fetch(tkn)

# initialize results list
results = list()

# iterate over inventory
for router in routers:
    # Use the appropriate network driver to connect to the device:
    if router['device_type'] == 'cisco':
        driver = napalm.get_network_driver("ios")
    else:
        break

    device = driver(
        hostname=router['ip_address'],
        username=user,
        password=None,
        optional_args={"use_keys": True, "key_file": "/home/onezero/.ssh/cisco"},
    )

    # open session to device
    print("Opening connection to {}".format(router['hostname']))
    device.open()
    # load config
    device.load_merge_candidate(config=config)

    # show diff
    print("\nDiff:")
    print(device.compare_config())

    # You can commit or discard the candidate changes.
    choice = input("\nWould you like to commit these changes? [yN]: ")
    if choice == "y":
        print("Committing ...")
        device.commit_config()
    else:
        print("Discarding ...")
        device.discard_config()

    # close the session with the device.
    device.close()
    print("Done.")
