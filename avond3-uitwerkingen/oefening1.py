'''
Oefening1: inventory-data uit netbox via 'requests'
'''
import get_inventory

tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'

# get data
output = get_inventory.fetch(tkn)

# display results
print("{:15}{:15}{:15}".format("Hostname", "Device Type", "IP Address"))
for router in output:
    print("{:15}{:15}{:15}".format(router['hostname'], router['device_type'], router['ip_address']))
