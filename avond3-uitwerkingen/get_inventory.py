'''
Tool om basis inventory informatie uit Netbox te krijgen

Usage: 
import get_inventory
get_inventory.fetch(token, device_role)

Returns list of devices:
- hostname: 'name'
  device_type: 'ios|eos|junos'
  ip_address: '<ip>'
'''

import requests
import json


def fetch(token, device_role='router'):
# set basic data
    authorization = 'Token {}'.format(token)
    application = 'application/json'

    # set headers
    headers = {
        'accept': application,
        'Authorization': authorization,
    }

    netbox_url = 'https://netbox.interestingtraffic.nl/api/'
    request_uri = 'dcim/devices'

    # get device list
    response = requests.get(netbox_url+request_uri, headers=headers)
    device_list = json.loads(response.text)

    # initialize results list
    results = list()

    # extract data
    for device in device_list['results']:
        if device['device_role']['slug'] == device_role:
            hostname = device['name']
            device_type = device['device_type']['manufacturer']['slug']
            full_address = device['primary_ip4']['address']
            ip_address = full_address.split('/')[0]
            router = {
                'hostname': hostname,
                'device_type': device_type,
                'ip_address': ip_address,
            }
            results.append(router)
    return results

if __name__ == '__main__':
    tkn = 'e67b72eb4f37b460d8001906049e4c85b85648ca'
    role = 'router'
    output = fetch(tkn, role)
    print(output)
