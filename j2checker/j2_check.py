"""
Simple Jinja2 checker, useful for testing jinja2 template syntax.
Looks for files with the .j2 extension in the provided directory and all subdirectories.

Usage: python3 j2_check.py <directory>
"""

import fnmatch
import os
import sys
import jinja2
from termcolor import colored


def find_files(target, extension):
    """
    find all *.j2 files in subdirectories
    """
    result = list()
    for root, _, filenames in os.walk(target):
        for filename in fnmatch.filter(filenames, extension):
            result.append(os.path.join(root, filename))
    return result


def jinja_loader(template):
    """
    load a Jinja2 tempate to check for syntax errors
    """
    template_dir = os.path.dirname(template)
    template_file = os.path.basename(template)
    j2_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir))
    try:
        j2_env.get_template(template_file)
        print(colored("{}: Template loaded successfully.".format(template_file), "green"))
        return 0
    except jinja2.exceptions.TemplateSyntaxError as exc:
        print(colored("Syntax check failed: {} in {} at line {}".format(
            exc.message, exc.filename, exc.lineno), "red"))
        return 1

if __name__ == "__main__":
    # find *.j2 files
    try:
        TARGETS = find_files(sys.argv[1], '*.j2')
    except IndexError:
        print("Usage: python3 j2_check.py <directory>")
        sys.exit(0)

    # check Jinja2 syntax for each file
    if len(TARGETS) == 0:
        print("No templates found in {}".format(sys.argv[1]))
        sys.exit(0)

    SUCCESS = True
    for j2_template in TARGETS:
        check = jinja_loader(j2_template)
        if check != 0:
            SUCCESS = False

    # exit program
    if SUCCESS:
        print(colored("\nAll templates loaded successfully.", "green"))
        sys.exit(0)
    else:
        print(colored("\nTemplate checking failed.", "red"))
        sys.exit(1)
