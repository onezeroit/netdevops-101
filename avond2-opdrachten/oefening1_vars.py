'''
Variabelen voor oefening 1

Importeer in je code met
  from oefening1_vars import input_vars

'''
input_vars = dict()
input_vars['hostname'] = 'RTR1'
input_vars['domain_name'] = 'onezero.lab'
input_vars['dns_server'] = '1.1.1.1'
input_vars['snmp_community'] = 'not_public'
input_vars['snmp_type'] = 'RO'
input_vars['snmp_trap_community'] = 'onezero_traps'
input_vars['snmp_trap_destination_1'] = '127.10.0.10'
input_vars['snmp_trap_destination_2'] = '127.10.0.11'
