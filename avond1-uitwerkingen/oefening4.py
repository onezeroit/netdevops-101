'''
Maak routers BGP peers
* push config
* check neighborship
* save config

'''
import time
from netmiko import Netmiko

## inventory
cisco_user = "onezero"
cisco_key_file = "/home/onezero1/.ssh/cisco"

RTR1 = {
    "host": "10.1.1.10",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}
RTR2 = {
    "host": "10.1.1.11",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}

# loop over routers to set config
for router in [RTR1,RTR2]:
    # generate commands list
    commands = [
        "router bgp 65000",
        "bgp router-id "+router['host'],
    ]
    if router['host'] == "10.1.1.10":
        commands.append("neighbor 10.1.1.11 remote-as 65000")
    else:
        commands.append("neighbor 10.1.1.10 remote-as 65000")
    print(router['host']+": commands: "+str(commands))
    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # send config
    print(router['host']+": sending config")
    net_connect.send_config_set(commands)
    # disconnect
    net_connect.disconnect()

# wait a bit, to allow neighborships to establish
delay = 20
print("waiting {} seconds to allow neighborships to establish".format(delay))
time.sleep(delay)

# confirm bgp neighborship
for router in [RTR1,RTR2]:
    # generate commands list
    if router['host'] == "10.1.1.10":
        command = ("show bgp neighbor 10.1.1.11")
    else:
        command = ("show bgp neighbor 10.1.1.10")

    print(router['host']+": verifing neighborship")
    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(command)
    if "Established" in output:
        print(router['host']+": neigborship established, saving config")
        net_connect.save_config()
    else:
         print(router['host']+": neigborship not ready, skipping")

    # disconnect
    net_connect.disconnect()
