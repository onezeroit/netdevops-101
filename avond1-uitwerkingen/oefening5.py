'''
Verzamel informatie
* hostname
* software versie
* uptime

Print in een tabel

'''

from netmiko import Netmiko
import re

## inventory
cisco_user = "onezero"
cisco_key_file = "/home/onezero1/.ssh/cisco"

RTR1 = {
    "host": "10.1.1.10",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}
RTR2 = {
    "host": "10.1.1.11",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}

command = "show version"

# regular expressions
regex_string = dict()
regex_string['hostname'] = r"(.+?) uptime is .+?"
regex_string['uptime'] = r".+? uptime is (.+?)\n"
regex_string['version'] = r".+? Software, Version (.+?)\n"
regex_attributes = ('hostname', 'uptime', 'version')

results = []
# loop over routers to set config
for router in [RTR1,RTR2]:
    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    print(router['host']+": collecting data")
    output = net_connect.send_command(command)
    # disconnect
    net_connect.disconnect()

    # parse 'show version' output
    print(router['host']+": parsing data")
    data = dict()
    for attribute in regex_attributes:
        data[attribute] = re.search(regex_string[attribute], output).group(1)
    
    # save data in results list
    results.append(data)

# print results
print()
print("------------ router inventory data ------------")
print("{:15}{:30}{:15}".format(*regex_attributes))
for router in results:
    print("{:15}{:30}{:15}".format(router['hostname'],router['uptime'],router['version']))
