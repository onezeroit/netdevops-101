'''
Maak een config backup van beide routers (RTR1, RTR2)
‘show run’
Sla op als <hostname>-<datum>.config

'''
from netmiko import Netmiko
from datetime import date

# inventory
cisco_user = "onezero"
cisco_key_file = "/home/onezero1/.ssh/cisco"

RTR1 = {
    "host": "10.1.1.10",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}
RTR2 = {
    "host": "10.1.1.11",
    "username": cisco_user,
    "use_keys": True,
    "key_file": cisco_key_file,
    "device_type": "cisco_ios",
}

# set date string
today = str(date.today())

# backup command
command = "show run"

# loop over routers
for router in [RTR1,RTR2]:
    # connect to router
    print(router['host']+": connecting to device")
    net_connect = Netmiko(**router)
    net_connect.find_prompt()
    # collect output
    output = net_connect.send_command(command)
    # disconnect
    net_connect.disconnect()

    # save config
    filename = router['host']+"-"+today+".config"
    print(router['host']+": saving config to "+filename)
    with open(filename, "w") as output_file:
        output_file.write(output)
