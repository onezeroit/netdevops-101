from netmiko import Netmiko

RTR1 = {
    "host": "10.1.1.10",
    "username": "onezero",
    "use_keys": True,
    "key_file": "/home/onezero1/.ssh/cisco",
    "device_type": "cisco_ios",
}

net_connect = Netmiko(**RTR1)
command = "show ip int brief"

print()
print(net_connect.find_prompt())
output = net_connect.send_command(command)
net_connect.disconnect()
print(output)
print()
