'''
Oefening 4: genereer configs twee switches

'''

from jinja_loader import jinja_render
import yaml

# variabelen
template_file = 'oefening4.j2'
common_vars_file = 'oefening4_vars_common.yml'
switch_vars_file = {
    'switch-1': 'oefening4_vars_switch1.yml',
    'switch-2': 'oefening4_vars_switch2.yml',
}
switches = ['switch-1','switch-2']

# render template
for switch in switches:
    # lees variabelen in
    with open(common_vars_file, 'r') as f:
        context = yaml.safe_load(f)

    # load switch vars
    with open(switch_vars_file[switch], 'r') as f:
        switch_vars = yaml.safe_load(f)

    # merge vars
    context.update(switch_vars)

    # render template
    output = jinja_render(template_file, context)

    # schrijf template weg
    rendered_config = 'output/{}.config'.format(switch)
    with open(rendered_config, 'w') as f:
        f.write(output)
