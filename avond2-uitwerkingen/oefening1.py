'''
Oefening 1: Jinja2 basics

Importeer variabelen en maak er een template van
'''

from jinja_loader import jinja_render
from oefening1_vars import input_vars # importeer variabelen

# variabelen
template_file = 'oefening1.j2'

# render template
output = jinja_render(template_file, input_vars)
print(output)
