from jinja2 import Template

name = input("Wat is je naam? ")
jinja = """
Hallo {{ name }}!

Je hebt zojuist een Jinja2 template gebruikt.

"""

template = Template(jinja)
output = template.render(name=name)

print(output)
