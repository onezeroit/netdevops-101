'''
Oefening 3: genereer configs voor hub en 3 spokes

'''

from jinja_loader import jinja_render
import yaml

# variabelen
template_file = 'oefening3.j2'
common_vars_file = 'oefening3_vars.yml'

# lees variabelen in
with open(common_vars_file, 'r') as f:
    context = yaml.safe_load(f)

# render template
for device in context['devices']:
    # merge vars
    context.update(device)
    output = jinja_render(template_file, context)
    rendered_config = "output/{}.config".format(device['name'])
    with open(rendered_config, 'w') as f:
        f.write(output)
